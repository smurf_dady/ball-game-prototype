﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace BallController.Input
{
    public class InputHandler : MonoBehaviour
    {
        public event Action<Vector2, float> OnStartTouch;
        public event Action<Vector2, float> OnEndTouch;
        
        private PlayerControls _playerControls;

        public float HorizontalInput => _playerControls.BallMovement.Movement.ReadValue<Vector2>().x;

        private void OnEnable()
        {
            _playerControls = new PlayerControls();
            _playerControls.Enable();
            
            _playerControls.BallMovement.PrimaryContact.started += StartTouchHandler;
            _playerControls.BallMovement.PrimaryContact.canceled += EndTouchHandler;
        }

        private void StartTouchHandler(InputAction.CallbackContext context)
        {
            OnStartTouch?.Invoke(Camera.main.ScreenToWorld(_playerControls.BallMovement.PrimaryPosition.ReadValue<Vector2>()),
                (float) context.startTime);
        }

        private void EndTouchHandler(InputAction.CallbackContext context)
        {
            OnEndTouch?.Invoke(Camera.main.ScreenToWorld(_playerControls.BallMovement.PrimaryPosition.ReadValue<Vector2>()), 
                (float) context.time);
        }
        private void OnDisable() => _playerControls.Disable();
    }
}