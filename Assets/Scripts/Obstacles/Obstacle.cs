using BallController;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Collisions.Obstacles
{
    public class Obstacle : MonoBehaviour, ICollision
    {
        [SerializeField] protected float _sizeValue;
        [SerializeField] protected int _scorePoints;

        private void Start()
        {
            _sizeValue = Random.Range(0.05f, 0.1f);
            _scorePoints = Random.Range(10, 50);
        }

        public virtual void Collide(Ball ball)
        {
            ball.ClingToBall(this,_sizeValue);
            ball.AddScore(_scorePoints);
        }
    }
}