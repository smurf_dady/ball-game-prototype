﻿using BallController;
using UnityEngine;
using Zenject;

namespace Collisions.Obstacles
{
    public class Human : Obstacle
    {
        [SerializeField] private Rigidbody[] _ragdollRigidbodies;
        
        private static readonly int IsRunning = Animator.StringToHash("isRunning");
        private Animator _animator;

        private bool _canMove = true;

        [Inject]
        private void Construct(Animator animator)
        {
            _animator = animator;
        }
        
        private void Awake()
        {
            SetRagdollActive(false, RigidbodyConstraints.None);
        }

        private void Update()
        {
            if(!_canMove) return;
            
            transform.Translate(Vector3.forward * 3f * Time.deltaTime);
            _animator.SetBool(IsRunning, true);
        }


        private void SetRagdollActive(bool isActive, RigidbodyConstraints constraints)
        {
            foreach (var rigidbody in _ragdollRigidbodies)
            {
                rigidbody.isKinematic = isActive;
                rigidbody.GetComponent<Collider>().isTrigger = isActive;
                rigidbody.mass = 0.05f;
                rigidbody.constraints = constraints;
            }
        }

        public override void Collide(Ball ball)
        {
            base.Collide(ball);
            
            _animator.SetBool(IsRunning,false);
            _canMove = false;
            
            SetRagdollActive(true, RigidbodyConstraints.FreezeAll);
            
        }
    }
}