using UnityEngine;
using Random = UnityEngine.Random;

namespace Obstacles.Data
{
    [CreateAssetMenu(fileName = "Obstacle Data", menuName = "Data / Obstacle Data")]
    public class ObstacleData : ScriptableObject
    {
        [SerializeField] private float _sizeValue;
        [SerializeField] private int _scorePoints;

        public float SizeValue => _sizeValue;

        public int ScorePoints => _scorePoints;

        private void OnEnable()
        {
            _sizeValue = Random.Range(0.05f, 0.1f);
            _scorePoints = Random.Range(10, 100);
        }
    }
}