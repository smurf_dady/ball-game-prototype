using BallController;

namespace Collisions
{
    public interface ICollision
    {
        void Collide(Ball ball);
    }
}