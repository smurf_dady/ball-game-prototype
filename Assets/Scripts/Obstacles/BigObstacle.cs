﻿using BallController;
using RayFire;
using UnityEngine;
using Zenject;

namespace Collisions.Obstacles
{
    [RequireComponent(typeof(RayfireRigid))]
    public class BigObstacle : Obstacle
    {
        private RayfireRigid _fireRigid;

        [Inject]
        private void Construct(RayfireRigid fireRigid)
        {
            _fireRigid = fireRigid;
        }

        public override void Collide(Ball ball)
        {
            base.Collide(ball);
            
            _fireRigid.Demolish();
            
            foreach (var fragment in _fireRigid.fragments)
            {
                fragment.gameObject.AddComponent<Obstacle>();
            }
        }
    }
}