using BallController.GameScore;
using BallController.Movement;
using Collisions.Obstacles;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace GameState
{
    public class WinGameTrigger : MonoBehaviour
    {
        [SerializeField] private Image _winImage;
        [SerializeField] private Image _loseImage;
        
        private void OnTriggerEnter(Collider other)
        {
            var human = other.GetComponent<Human>();
            if(human)
                Destroy(human.gameObject);
            
            var ball = other.GetComponent<BallScore>();
            if (!ball) return;
            
            if(ball.IsReachedTarget)
                _winImage.gameObject.SetActive(true);
            else _loseImage.gameObject.SetActive(true);
            
            ball.GetComponent<BallMovement>().StopMove();
        }

        public void RestartLevel() => SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}