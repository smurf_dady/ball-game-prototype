using BallController.GameScore;
using BallController.Size;
using Collisions.Obstacles;
using UnityEngine;
using Zenject;

namespace BallController
{
    public class Ball : MonoBehaviour
    {
        private BallSize _ballSize;
        private BallScore _ballScore;

        [Inject]
        private void Construct(BallSize ballSize, BallScore ballScore)
        {
            _ballSize = ballSize;
            _ballScore = ballScore;
        }

        public void ClingToBall(Obstacle obstacle ,float sizeValue)
        {
            obstacle.transform.SetParent(transform);
            obstacle.GetComponent<Collider>().isTrigger = true;
            
            var rigidbody = obstacle.GetComponent<Rigidbody>();
            if(rigidbody != null)
             rigidbody.constraints= RigidbodyConstraints.FreezeAll;
            
            _ballSize.Size += new Vector3(sizeValue, sizeValue, sizeValue);
        }

        public void AddScore(int score)
        {
            _ballScore.Score += score;
        }
    }
}