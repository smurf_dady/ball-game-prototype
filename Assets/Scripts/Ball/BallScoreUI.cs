using System;
using TMPro;
using UnityEngine;
using Zenject;

namespace BallController.GameScore.UI
{
    public class BallScoreUI : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _scoreText;
        [SerializeField] private TextMeshProUGUI _targetScoreText;

        private BallScore _ballScore;

        [Inject]
        private void Construct(BallScore ballScore)
        {
            _ballScore = ballScore;
        }

        private void Start()
        {
            _scoreText.text = $"Score:{_ballScore.Score}";
            _targetScoreText.text = $"Target:{_ballScore.TargetScore}";

            _ballScore.OnScoreChanged += UpdateScore;
        }
        
        private void UpdateScore() => _scoreText.text = $"Score:{_ballScore.Score}";

        private void OnEnable() => _ballScore.OnScoreChanged -= UpdateScore;
    }
}