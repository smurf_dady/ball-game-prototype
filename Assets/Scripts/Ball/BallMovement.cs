﻿using BallController.Data;
using BallController.Input;
using UnityEngine;
using Zenject;


namespace BallController.Movement
{
    public class BallMovement : MonoBehaviour
    {
        [SerializeField] private BallData _data;

        private InputHandler _inputHandler;
        private Rigidbody _rigidbody;

        [Inject]
        private void Construct(InputHandler inputHandler, Rigidbody rigidbody)
        {
            _inputHandler = inputHandler;
            _rigidbody = rigidbody;
        }

        private void Update()
        {
           MoveInput(_inputHandler.HorizontalInput);
        }

        public void MoveInput(float inputValue)
        {
            _rigidbody.AddForce(new Vector3(
                inputValue,
                0f,
                0f) * _data.RollSpeed,ForceMode.Impulse);
        }

        public void StopMove()
        {
            _rigidbody.constraints = RigidbodyConstraints.FreezeAll;

            enabled = false;
        } 
        
        private void FixedUpdate()
        {
            var velocity = _rigidbody.velocity;
            velocity = new Vector3(velocity.x, velocity.y, _data.MoveSpeed);
            _rigidbody.velocity = velocity;
        }
    }
}