using System;
using UnityEngine;

namespace BallController.Size
{
    public class BallSize : MonoBehaviour
    {
        public event Action OnSizeChanged;
        
        private Vector3 _size;

        public Vector3 Size
        {
            get => _size;   
            set
            {
                _size = value;
                OnSizeChanged?.Invoke();
            }
        }

        private void Start()
        {
            _size = transform.localScale;

            OnSizeChanged += SizeChangedHandler;
        }

        private void SizeChangedHandler() => transform.localScale = _size;

        private void OnDisable() => OnSizeChanged -= SizeChangedHandler;
    }
}