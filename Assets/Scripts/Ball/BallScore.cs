using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace BallController.GameScore
{
    public class BallScore : MonoBehaviour
    {
        public event Action OnScoreChanged;

        [SerializeField]private int _score;
        [SerializeField]private int _targetScore;

        public int Score
        {
            get => _score;
            set
            {
                _score = value;
                if (_score >= _targetScore)
                    IsReachedTarget = true;
                
                OnScoreChanged?.Invoke();
            }
        }

        public int TargetScore => _targetScore;

        public bool IsReachedTarget { get; private set; }
        

        private void Awake()
        {
            _targetScore = Random.Range(300, 900);
        }
    }
}