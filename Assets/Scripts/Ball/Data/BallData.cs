﻿using System;
using UnityEngine;

namespace BallController.Data
{
    [CreateAssetMenu(fileName = "Ball Data", menuName = "Data / Ball Data")]
    public class BallData : ScriptableObject
    {
        [SerializeField] private float _moveSpeed;
        [SerializeField] private float _rollSpeed;

        public float MoveSpeed => _moveSpeed;

        public float RollSpeed => _rollSpeed;
        
    }
}