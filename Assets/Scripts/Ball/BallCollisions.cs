﻿using Collisions;
using UnityEngine;
using Zenject;

namespace BallController.Collisions
{
    public class BallCollisions : MonoBehaviour
    {
        private ICollision _collision;
        private Ball _ball;

        [Inject]
        private void Construct(Ball ball)
        {
            _ball = ball;
        }

        private void OnCollisionEnter(Collision other)
        {
            var collision = other.gameObject.GetComponent<ICollision>();
            if (collision == null) return;

            _collision = collision;
            
            _collision.Collide(_ball);
        }

        private void OnCollisionExit(Collision other)
        {
            var collision = other.gameObject.GetComponent<ICollision>();
            if (_collision != collision) return;

            _collision = null;
        }
    }
}