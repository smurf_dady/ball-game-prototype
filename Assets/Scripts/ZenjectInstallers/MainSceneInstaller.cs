﻿using BallController;
using BallController.GameScore;
using BallController.GameScore.UI;
using BallController.Input;
using BallController.Size;
using BallController.Movement;
using RayFire;
using UnityEngine;
using Zenject;

namespace ZenjectInstallers
{
    public class MainSceneInstaller : MonoInstaller
    {
        [SerializeField] private InputHandler _inputHandler;
        [SerializeField] private Ball _ball;
        [SerializeField] private BallScoreUI _balScoreUI;
        [SerializeField] private BallScore _ballScore;

        public override void InstallBindings()
        {
            BindBaseComponents();
            BindInputHandler();
            BindBall();
            BindRayFire();
        }

        private void BindRayFire()
        {
            Container.Bind<RayfireRigid>().FromComponentSibling().AsTransient();
        }

        private void BindBall()
        {
            Container.Bind<BallSize>().FromComponentOn(_ball.gameObject).AsSingle();
            Container.Bind<BallMovement>().FromComponentOn(_ball.gameObject).AsSingle();
            Container.Bind<Ball>().FromInstance(_ball).AsSingle();
            Container.Bind<BallScore>().FromInstance(_ballScore).AsSingle();
        }

        private void BindInputHandler()
        {
            Container.Bind<InputHandler>().FromInstance(_inputHandler).AsSingle();
        }

        private void BindBaseComponents()
        {
            Container.Bind<Rigidbody>().FromComponentSibling().AsTransient();
            Container.Bind<Animator>().FromComponentSibling().AsTransient();
            Container.Bind<Collider>().FromComponentSibling().AsTransient();
        }
    }
}
